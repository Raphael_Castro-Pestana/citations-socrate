# citations-socrate

Citation 1 : Tout ce que je sais, c'est que je ne sais rien, tandis que les autres croient savoir ce qu'ils ne savent pas.

Citation 2 : Existe-t-il pour l'homme un bien plus précieux que la Santé ?

Citation 3 : Je ne suis ni Athénien, ni Grec, mais un citoyen du monde.

Citation 4 : Connais-toi toi-même.

